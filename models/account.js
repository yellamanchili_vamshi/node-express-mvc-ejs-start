/** 
*  Account model
*  Describes the characteristics of each attribute in an account esource.
*
* @author Vamshi Krishna Yellamanchili<S534746@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const account = new mongoose.Schema({
  accountnumber: { type: Number, required: true },
  firstname: { type: String, required: true, default: 'Given' },
  middlename: { type: String, required: false, default: 'Given' },
  familyname: { type: String, required: true, default: 'Family' },
  accounttype: {type:String, required: false, default:'Checking'},
  transactionid: { type: Number, required: true, default: '' },
  accountbalance: {type: Number,required:true}

})

module.exports = mongoose.model('Account', accountSchema)